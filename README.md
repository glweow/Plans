
# Планы и информация // weow.
Этот репозиторий содержит всякую информацию и планы weow.

Файл README.md содержит информацию о weow., когда остальные - планы.

# Информация
## Значки
Чаще всего, в наших проектах будут вот эти значки:

[![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg?)](https://github.com/tterb/atomic-design-ui/blob/master/LICENSEs)

[![Developed by weow.](https://img.shields.io/badge/Developed%20by-weow.-informational)](https://discord.gg/8KxdUfAzbE)

Первый из них обозначает лицензию, а именно, в данном случае, MIT (X11).

Второй из них является чисто Ватермаркой для наших репозиториев и ведёт на наш Discord сервер.
## Люди в команде.
### Leader.
- [latte@mastodon.ml](https://mastodon.ml/@latte)
### Devs.
#### BE:
(Пока нет) /h
#### FE:
(Пока нет) /h
### Testers.
(Пока нет) /h
### PR Managers.
(Пока нет) /h
### Mastodon Account Manager.
(Пока нет) /h
### Mods.
snyxok#5014
/h
#### sidenote: если где-то написано /h - значит на роль можно [подать](https://forms.gle/LDqdBnPeHGcAEi8KA) заявку.
